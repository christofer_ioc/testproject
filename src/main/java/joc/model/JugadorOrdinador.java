/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package joc.model;

/**
 *
 * @author christoferbenavides
 */

import java.util.Random;

public class JugadorOrdinador implements EleccioJugador {
    @Override
    public String fesEleccio() {
        String[] eleccions = {"pedra", "paper", "tisora"};
        Random random = new Random();
        return eleccions[random.nextInt(eleccions.length)];
    }
}

