/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package joc.model;

/**
 *
 * @author christoferbenavides
 */
public class Joc {
    private EleccioJugador jugador;
    private EleccioJugador jugadorOrdinador;

    public Joc(EleccioJugador jugador, EleccioJugador jugadorOrdinador) {
        this.jugador = jugador;
        this.jugadorOrdinador = jugadorOrdinador;
    }

    public void iniciaJoc() {
        System.out.println("Benvingut al joc de Pedra, Paper o Tisora!");
        System.out.println("Entra la teva elecció: pedra, paper o tisora");

        String eleccioJugador = jugador.fesEleccio();
        String eleccioOrdinador = jugadorOrdinador.fesEleccio();

        System.out.println("El jugador ha triat: " + eleccioJugador);
        System.out.println("L'ordinador ha triat: " + eleccioOrdinador);

        String resultat = determinaGuanyador(eleccioJugador, eleccioOrdinador);
        System.out.println("Resultat: " + resultat);
    }

    private String determinaGuanyador(String eleccioJugador, String eleccioOrdinador) {
        if (eleccioJugador.equals(eleccioOrdinador)) {
            return "Empat!";
        } else if (
            (eleccioJugador.equals("pedra") && eleccioOrdinador.equals("tisora")) ||
            (eleccioJugador.equals("paper") && eleccioOrdinador.equals("pedra")) ||
            (eleccioJugador.equals("tisora") && eleccioOrdinador.equals("paper"))
        ) {
            return "Has guanyat!";
        } else {
            return "L'ordinador guanya!";
        }
    }
}

