/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package joc.model;

/**
 *
 * @author christoferbenavides
 */
import java.util.Scanner;

public class Jugador implements EleccioJugador {
    @Override
    public String fesEleccio() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine().toLowerCase();
    }
}

